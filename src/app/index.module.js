(function() {
  'use strict';

  angular
    .module('rmittimetableConverter', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ui.router', 'ui.bootstrap', 'toastr','ngTable','ngCsv','ngStorage']);

})();
