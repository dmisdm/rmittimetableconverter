(function() {
  'use strict';

  angular
    .module('rmittimetableConverter')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/:html',
        template:'<main></main>',
        data: {
          auth: {
            allowedRoles: [

            ]
          }
        }
      });

    $urlRouterProvider.otherwise('/');
  }

})();
