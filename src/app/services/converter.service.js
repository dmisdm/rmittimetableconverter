/**
 * Created by Daniel on 19/02/2016.
 */
(function() {

  angular.module('rmittimetableConverter')
    .service('ConverterService', ConverterService);


  /* @ngInject */
  function ConverterService( assert ,_, moment, $log, cheerio) {



    var svc = {};


    function convertHtml(html, numOfWeeks, courseTitles, hiddenCourses) {

      hiddenCourses = hiddenCourses || {};

      if(!numOfWeeks)numOfWeeks = 1;


       courseTitles = courseTitles || {}
      ;

      var days = [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday'
      ];




      var outputRows = [];

      var $html = cheerio.load(html);

      var weeks = $html('table.datatable tbody').children();
      weeks.splice(0,2);
      weeks.each(process_row);




      function process_row(i, row) {
        var $html = cheerio.load(row);
        $html('td[class^="eventbg"]').each(function(i,elem) {

          var payload = {
            day:elem.parent.children[0].children[0].data.trim(),
            start:moment(elem.children[0].attribs.title.split('\n')[0].split(' ')[2],"HH:mm"),
            end:moment(elem.children[0].attribs.title.split('\n')[0].split(' ')[4],"HH:mm"),
            name:elem.children[0].attribs.title.split('\n')[1].trim(),
            courseCode:elem.children[0].attribs.title.split('\n')[1].trim().split('-').slice(0,2).join('-'),
            courseTitle:courseTitles[elem.children[0].attribs.title.split('\n')[1].trim().split('-').slice(0,2).join('-')] || elem.children[0].attribs.title.split('\n')[1].trim().split('-').slice(0,2).join('-'),
            location: elem.children[0].attribs.title.split('\n')[2].trim(),
            type:elem.children[0].attribs.title.split('\n')[3].trim()

          };

          if(hiddenCourses[payload.courseCode]) return;



          for(var x = 0; x < numOfWeeks; x++)
          {
            var startDate = moment().day(days.indexOf(payload.day) + (x * 7));
            outputRows.push({
              Subject:payload.courseTitle,
              'Start Date':startDate.format("M/D/YYYY"),
              'Start Time':payload.start.format("h:mm a"),
              'End Date':startDate.format("M/D/YYYY"),
              'End Time':payload.end.format("h:mm a"),
              Description: payload.type,
              Location: payload.location

            });



          }

        });


      }


      return outputRows;


    }





    svc.convertHtml = convertHtml;


    return svc;


  }

})();
