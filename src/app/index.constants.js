/* global require:false, moment:false, _:false, assert:false */
(function() {
  'use strict';


  var cheerio = require('cheerio');
  var csv = require('csv');



  angular
    .module('rmittimetableConverter')
    .constant('moment', moment)
    .constant('AUTH_ROLES', {
      anonymous:'anonymous',
      user:'user',
      admin:'admin'
    })
    .constant('cheerio',cheerio)
    .constant('csv',csv)
    .constant('_',_)
    .constant('assert',assert);

})();
