(function() {

  angular.module('rmittimetableConverter')
    .component('main', {
      controller:MainController,
      controllerAs:'vm',
      templateUrl:'app/main/main.html'
    });

  /* @ngInject */
  function MainController($log, $scope, ConverterService, $localStorage, $stateParams, NgTableParams, csv, $window, $document, _) {

    var vm = this;

    vm.tableHtml = $localStorage.tableHtml;

    vm.numberOfWeeks = 10;

    vm.data = [];

    vm.subjectMappings ={

      };

    vm.hiddenCourses = {};

    vm.changedHiddenCourse = function() {

      refreshData();
    }

    if($stateParams.html) {
      var decoded = atob($stateParams.html);
      vm.tableHtml = decoded;
    }

    vm.tableConfig = new NgTableParams({

    }, {
      data:vm.data
    });



    vm.subjectMappingKeys = function() {

      return Object.keys(vm.subjectMappings);
    };




    function refreshData()
    {
      $localStorage.tableHtml = vm.tableHtml;

      var data = ConverterService.convertHtml(vm.tableHtml,vm.numberOfWeeks,vm.subjectMappings,vm.hiddenCourses);
      vm.data = data;


      if(vm.subjectMappingKeys().length == 0 && data.length != 0)
      {
        _.forEach(_.groupBy(data,'Subject'),function(value,key) {
          if(!vm.subjectMappings[key]) {

            vm.subjectMappings[key] = key;
          }
        });

      }




    }

    vm.changedSubjectMapping = function() {
      refreshData();
      //$localStorage.subjectMappings = vm.subjectMappings;

    }

    $scope.$watch('vm.subjectMappings',function() {


    });

    $scope.$watch('vm.numberOfWeeks',refreshData);


    $scope.$watch('vm.tableHtml', refreshData);


    vm.processAndDownload = function() {

      csv.stringify(vm.data, {
        //columns:Object.keys(vm.data[0])
        header:true
      },function(err, output) {
        exportCsv(output);
      })


    };




    vm.bookmark = function() {



    }



    function exportCsv(string) {
      var today = new Date();
      var fileName = 'TimeTableExport' + today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate() + '-' + today.getHours() + ':' + today.getMinutes()+ '.csv';
      var a = $document.createElement('a');
      a.href = 'data:attachment/csv,' + encodeURIComponent(string);
      a.target = '_blank';
      a.download = fileName;
      $document.body.appendChild(a);
      a.click();
    }


  }



})();
