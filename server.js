/**
 * Created by danie on 18/02/2016.
 */
(function () {

  var express = require('express');
  var app = express();


  app.use('/',express.static('dist'));

  app.listen(process.env.PORT || 5000, function() {
    console.log("Started Server on port: %s", process.env.PORT || 5000 );
  });


})();
